from flask import Flask, render_template, request, redirect, url_for, session, flash
import pymysql.cursors
import re

app = Flask(__name__)

app.secret_key = 'qwerty'

app.config['MYSQL_HOST'] = 'localhost'
app.config['MYSQL_USER'] = 'root'
app.config['MYSQL_PASSWORD'] = ''
app.config['MYSQL_DB'] = 'py_api'

mysql = pymysql.connect(
    host=app.config['MYSQL_HOST'],
    user=app.config['MYSQL_USER'],
    password=app.config['MYSQL_PASSWORD'],
    db=app.config['MYSQL_DB'],
    cursorclass=pymysql.cursors.DictCursor
)

@app.route('/')
@app.route('/login', methods=['GET', 'POST'])
def login():
    if 'email' in session:
        return redirect(url_for('manage_patients'))

    message = ''
    if request.method == 'POST' and 'email' in request.form and 'password' in request.form:
        email = request.form['email']
        password = request.form['password']
        cursor = mysql.cursor()
        cursor.execute('SELECT * FROM users WHERE email = %s AND password = %s', (email, password,))
        user = cursor.fetchone()
        if user:
            session['email'] = user['email']
            flash('Logged in successfully!', 'success')
            print("User logged in successfully")
            return redirect(url_for('manage_patients'))
        else:
            message = 'Please enter correct email / password!'
            print("Login failed")
    return render_template('login.html', message=message)

@app.route('/logout')
def logout():
    session.pop('email', None)
    flash('You have been logged out', 'info')
    return redirect(url_for('login'))

@app.route('/register', methods=['GET', 'POST'])
def register():
    message = ''
    if request.method == 'POST' and 'name' in request.form and 'password' in request.form and 'email' in request.form:
        userName = request.form['name']
        password = request.form['password']
        email = request.form['email']
        cursor = mysql.cursor()
        cursor.execute('SELECT * FROM users WHERE email = %s', (email,))
        account = cursor.fetchone()
        if account:
            message = 'Account already exists!'
        elif not re.match(r'[^@]+@[^@]+\.[^@]+', email):
            message = 'Invalid email address!'
        elif not userName or not password or not email:
            message = 'Please fill out the form!'
        else:
            cursor.execute('INSERT INTO users (name, email, password) VALUES (%s, %s, %s)', (userName, email, password))
            mysql.commit()
            message = 'You have successfully registered!'
            flash(message, 'success')
    elif request.method == 'POST':
        message = 'Please fill out the form!'
    return render_template('register.html', message=message)
@app.route('/user', methods=['GET', 'POST'])
def manage_patients():
    if 'email' not in session:
        return redirect(url_for('login'))

    cursor = mysql.cursor()

    if request.method == 'POST':
        patient_name = request.form['patient_name']
        guardian_name = request.form['guardian_name']
        patient_number = request.form['patient_number']
        patient_date = request.form['patient_date'] 

        cursor.execute('INSERT INTO patients (name, guardian_name, patient_number, date) VALUES (%s, %s, %s, %s)', 
                       (patient_name, guardian_name, patient_number, patient_date))
        mysql.commit()
        flash('Patient added successfully', 'success')

    cursor.execute('SELECT * FROM patients')
    patients = cursor.fetchall()
    return render_template('user.html', patients=patients)

@app.route('/user/update/<int:patient_id>', methods=['GET', 'POST'])
def update_patient(patient_id):
    if 'email' not in session:
        return redirect(url_for('login'))

    cursor = mysql.cursor()
    cursor.execute('SELECT * FROM patients WHERE id = %s', (patient_id,))
    patient = cursor.fetchone()

    if request.method == 'POST':
        patient_name = request.form['edited_name']
        guardian_name = request.form['edited_guardian_name']
        patient_number = request.form['edited_patient_number']
        patient_date = request.form['edited_date']  

        cursor.execute('UPDATE patients SET name = %s, guardian_name = %s, patient_number = %s, date = %s WHERE id = %s',  
                       (patient_name, guardian_name, patient_number, patient_date, patient_id))
        mysql.commit()
        flash('Patient updated successfully', 'success')

        return redirect(url_for('manage_patients'))


@app.route('/user/delete/<int:patient_id>', methods=['POST'])
def delete_patient(patient_id):
    if 'email' not in session:
        return redirect(url_for('login'))

    cursor = mysql.cursor()
    cursor.execute('DELETE FROM patients WHERE id = %s', (patient_id,))
    mysql.commit()
    flash('Patient deleted successfully', 'success')

    return redirect(url_for('manage_patients'))

if __name__ == "__main__":
    app.run(debug=True)
